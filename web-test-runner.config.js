import { playwrightLauncher } from "@web/test-runner-playwright";

export default {
  browsers: [
    playwrightLauncher({
      product: "chromium",
      launchOptions: {
        executablePath: "./.chromium",
      },
    }),
  ],
  files: ["test/unit.js"],
  nodeResolve: true,
};
