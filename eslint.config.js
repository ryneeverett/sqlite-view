import globals from "globals";
import pluginJs from "@eslint/js";

import jest from "eslint-plugin-jest";
import playwright from "eslint-plugin-playwright";

export default [
  { ignores: ["dist/", "examples/sqlite-viewer"] },

  pluginJs.configs.recommended,
  { languageOptions: { globals: globals.browser } },

  {
    files: ["vite.config.js"],
    languageOptions: {
      globals: {
        __dirname: "readonly",
      },
    },
  },

  // Integration Tests (commonjs)
  {
    files: ["test/infra.js", "test/integration.test.js"],
    plugins: { jest },
    languageOptions: {
      globals: {
        ...globals.node,
        ...jest.environments.globals.globals,
        page: "readonly",
        jestPlaywright: "readonly",
      },
    },
    ...playwright.configs["flat/jest-playwright"],
  },

  // Unit Tests (chai)
  {
    files: ["test/unit.js"],
    languageOptions: {
      globals: {
        describe: "readonly",
        it: "readonly",
      },
    },
  },
];
