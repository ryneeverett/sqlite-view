const fs = require("node:fs");
const http = require("node:http");
const path = require("node:path");

const finalhandler = require("finalhandler");
const portfinder = require("portfinder");
const serveStatic = require("serve-static");

class ExampleDir {
  constructor(root) {
    this.path = root != null ? root : fs.mkdtempSync("integration-test-tmp-");
    fs.mkdirSync(path.join(this.path, "src"));
    fs.copyFileSync(
      "./dist/sqlite-view.js",
      path.join(this.path, "src/index.js"),
    );
    fs.cpSync("./examples", path.join(this.path, "examples"), {
      recursive: true,
    });
  }

  teardown() {
    fs.rm(this.path, { recursive: true }, (err) => {
      return err;
    });
  }
}

class ExampleServer {
  constructor() {
    this.example = new ExampleDir();
    const serve = serveStatic(this.example.path);
    this.server = http.createServer((req, res) =>
      serve(req, res, finalhandler(req, res)),
    );
    this.address = "127.0.0.1";
  }

  async start() {
    this.port = await portfinder.getPortPromise();
    this.server.listen(this.port);
  }

  stop() {
    this.server.close();
    this.example.teardown();
  }

  request(path) {
    return new Promise((resolve) => {
      const req = http.request(
        {
          hostname: this.address,
          port: this.port,
          path,
          method: "GET",
        },
        (res) => resolve(res),
      );
      req.end();
    });
  }

  goto(path) {
    return page.goto(`http://${this.address}:${this.port}${path}`);
  }
}

module.exports = {
  makeExamples: ExampleDir,
  server: new ExampleServer(),
};
