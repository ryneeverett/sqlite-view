import { expect } from "@esm-bundle/chai";

import SqliteView from "../dist/sqlite-view.js";

// Adapted from https://github.com/sql-js/sql.js/issues/284#issuecomment-767119586
async function initSqlJs() {
  const fileMap = (file) => `node_modules/sql.js/dist/${file}`;

  // Get the SqlJs code as a string.
  const response = await fetch(fileMap("sql-wasm.js"));
  const code = await response.text();

  // Instantiate the code and access its exports.
  const f = new Function("exports", code);
  const exports = {};
  f(exports);

  // Call the real initSqlJs().
  return exports.Module({ locateFile: fileMap });
}

describe("security", () => {
  it("html escaped", async () => {
    const SQL = await initSqlJs();
    const db = new SQL.Database();
    db.run("CREATE TABLE foo (bar char);");
    db.run("INSERT INTO foo VALUES ('<p>hello world</p>');");
    const dbBytes = db.export().buffer;

    const element = document.createElement("div");
    element.setAttribute("id", "someid");
    document.body.append(element);

    const sv = new SqliteView("someid");
    await sv.load(dbBytes);
    expect(sv.element.querySelector("td").innerHTML).to.equal(
      '<span title="<p>hello world</p>">&lt;p&gt;hello world&lt;/p&gt;</span>',
    );
  });
});
