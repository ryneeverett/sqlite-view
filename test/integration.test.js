const { server } = require("./infra.js");

beforeAll(async () => {
  console.log(`Running tests on ${process.env.BROWSERS || "chromium"}.`);
  await server.start();
});
afterAll(() => {
  server.stop();
});

describe("server works and pages load", () => {
  beforeEach(async () => {
    // Clear cache to ensure we get 200's rather than 304's.
    await jestPlaywright.resetContext();
    // Fail if there are any console logs. Ideally this would be a separate test.
    page.on("console", (msg) => expect(msg).toBeNull());
  });

  function recordFailures() {
    const failures = [];
    page.on("requestfailed", (request) => {
      failures.push(`FAIL ${request.url()} ${request.failure().errorText}`);
    });
    page.on("response", (response) => {
      if (response.status() !== 200) {
        failures.push(
          `${response.status()} ${response.url()} ${response.statusText()}`,
        );
      }
    });
    return failures;
  }

  test("index is served", async () => {
    const res = await server.request("/examples/");
    expect(res.statusCode).toBe(200);
  });
  test("vanilla loads cleanly in browser", async () => {
    const failures = recordFailures();
    await server.goto("/examples/vanilla.html");
    expect(failures).toStrictEqual([]);
  });
  test("sqlite-viewer loads cleanly in browser", async () => {
    const failures = recordFailures();
    await server.goto("/examples/sqlite-viewer/");
    expect(failures).toStrictEqual([]);
  });
});

describe("basic functionality", () => {
  const rowSelector = ".sqlview-table-container table > tbody > tr";

  test("css injected", async () => {
    await server.goto("/examples/vanilla.html");
    const aceCursorStyle = await page.$eval(".ace_hidden-cursors", (node) =>
      window.getComputedStyle(node),
    );
    expect(aceCursorStyle.opacity).toBe("0");
  });
  test("data loaded into table", async () => {
    server.goto("/examples/vanilla.html");
    await page.waitForSelector(rowSelector);
    const tableRows = await page.$$(rowSelector);
    expect(tableRows.length).toBe(30);
  });
  test("reload data into table", async () => {
    await server.goto("/examples/test/reload.html");
    const body = await page.waitForSelector("body[data-pass]");
    const testPassed = await body.getAttribute("data-pass");
    expect(testPassed).toBe("true");
  });
  test("table selector", async () => {
    await server.goto("/examples/vanilla.html");
    await page.click('.sqlview-table-select-container div[data-value="Album"]');
    await page.click('.sqlview-table-select-container div[data-value="Track"]');
    const columnHeaders = await page.$$(
      ".sqlview-table-container table > thead > tr > th > span",
    );
    const firstColumnName = await columnHeaders[0].innerHTML();
    expect(firstColumnName).toBe("TrackId");
  });
  test("sql editor and button", async () => {
    await server.goto("/examples/vanilla.html");
    await page.waitForSelector(".sqlview-editor-container > .ace_editor");
    await page.$eval(
      ".sqlview-editor-container > .ace_editor",
      async (node) => {
        const editor = window.ace.edit(node.id);
        // XXX I suspect we're waiting here for ace to be initialized. It would be better if ace had a
        // 'ready' event, an editor.initialized property or a standardized way to tell if the editor
        // is fully initialized. I could probably figure this out by looking at how ace tests itself.
        function sleep(ms) {
          return new Promise((resolve) => {
            setTimeout(resolve, ms);
          });
        }
        await sleep(500);
        editor.setValue(editor.getValue().replace("30", "50"));
      },
    );
    const runButton = await page.waitForSelector(
      ".sqlview-run-button-container > button",
    );
    await runButton.click();
    await page.waitForSelector(rowSelector);
    const tableRows = await page.$$(rowSelector);
    expect(tableRows.length).toBe(50);
  });
  test("pagination", async () => {
    await server.goto("/examples/vanilla.html");
    const nextButton = await page.waitForSelector(".sqlview-page-next");
    await nextButton.click();
    const tableCells = await page.$$(
      ".sqlview-table-container table > tbody > tr > td > span",
    );
    const firstId = await tableCells[0].innerHTML();
    expect(firstId).toBe("31");
  });
  test("choicesConfig", async () => {
    await server.goto("/examples/test/choices-config.html");
    const choicesContainer = await page.waitForSelector(
      ".sqlview-table-select-container > .test-choices-container ",
    );
    expect(choicesContainer).not.toBe(null);
  });
});

describe("return values", () => {
  // Unit tests using jsdom would be more appropriate than integration tests using playwright, but
  // this would be a pain since we'd have to import the ecmascript source into commonjs which has
  // limited support at this point.
  beforeEach(async () => {
    // Clear cache to ensure we get 200's rather than 304's.
    await jestPlaywright.resetContext();
  });

  test("asynchronous load", async () => {
    await server.goto("/examples/test/async.html");
    const body = await page.waitForSelector("body[data-rows]");
    const rows = await body.getAttribute("data-rows");
    expect(rows).toBe("0");
  });

  test("await load promise", async () => {
    await server.goto("/examples/test/await.html");
    const body = await page.waitForSelector("body[data-rows]");
    const rows = await body.getAttribute("data-rows");
    expect(rows).toBe("30");
  });

  test("constructor return existing instance", async () => {
    await server.goto("/examples/test/existing.html");
    const body = await page.waitForSelector("body[data-pass]");
    const testPassed = await body.getAttribute("data-pass");
    expect(testPassed).toBe("true");
  });
});

describe("css", () => {
  test("class injection before load", async () => {
    await server.goto("/examples/sqlite-viewer/");
    const bootstrapContainer = await page.$(".container");
    expect(bootstrapContainer).not.toBe(null);
  });
  test("override injected styles", async () => {
    await server.goto("/examples/test/css-override.html");
    const tableStyle = await page.$eval(
      ".sqlview-table-container table",
      (node) => window.getComputedStyle(node),
    );
    expect(tableStyle.borderStyle).toBe("none");
  });
});
