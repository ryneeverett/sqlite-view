export default function html(editorId) {
  return `
    <div class="sqlview-main-container">
        <div>

            <div>
                <div class="sqlview-table-select-container"></div>

                <div class="sqlview-editor-container">
                    <div id="${editorId}" contenteditable="true"></div>
                </div>

                <div class='sqlview-run-button-container'>
                    <button type="submit">Execute</button>
                </div>

                <div class="sqlview-table-container">

                    <div style="overflow-x: auto">
                        <table>
                            <thead>
                            <tr></tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                    <div class="sqlview-error" style="display: none"></div>

                </div>

            </div>

        </div>
    </div>

    <div class="sqlview-bottom-bar">
        <div class="sqlview-inline">
            <button class="sqlview-page-prev gg-chevron-left" type="submit"></button>
            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" class="sqlview-pager"></a>
            <button class="sqlview-page-next gg-chevron-right" type="submit"></button>
        </div>
    </div>
  `;
}
