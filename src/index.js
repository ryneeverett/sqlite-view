import * as ace from "ace-builds";
import "ace-builds/src-noconflict/mode-sql";
import "ace-builds/src-noconflict/theme-chrome";

import Choices from "choices.js";

import initSqlJs from "sql.js/dist/sql-wasm.js";
import sqljsPath from "sql.js/dist/sql-wasm.wasm?url";

import html from "./html.js";
import styles from "./styles.css?inline";

const SQL_FROM_REGEX = /FROM\s+([^\s;]+)/im;
const SQL_LIMIT_REGEX = /LIMIT\s+(\d+)(?:\s*,\s*(\d+))?/im;
const SQL_SELECT_REGEX = /SELECT\s+[^;]+\s+FROM\s+/im;

let db = null;
let bottomBarDefaultPos = null;
let bottomBarDisplayStyle = null;
const lastCachedQueryCount = {};
const instances = {};

function escapeHTML(str) {
  // Copied from https://stackoverflow.com/a/22706073.
  const p = document.createElement("p");
  p.appendChild(document.createTextNode(str));
  return p.innerHTML;
}

function getTableRowsCount(name) {
  const sel = db.prepare(`SELECT COUNT(*) AS count FROM '${name}'`);
  if (sel.step()) {
    return sel.getAsObject().count;
  }
  return -1;
}

function getQueryRowCount(query) {
  if (query === lastCachedQueryCount.select) {
    return lastCachedQueryCount.count;
  }

  let queryReplaced = query.replace(
    SQL_SELECT_REGEX,
    "SELECT COUNT(*) AS count_sv FROM ",
  );

  if (queryReplaced !== query) {
    queryReplaced = queryReplaced.replace(SQL_LIMIT_REGEX, "");
    const sel = db.prepare(queryReplaced);
    if (sel.step()) {
      const count = sel.getAsObject().count_sv;

      lastCachedQueryCount.select = query;
      lastCachedQueryCount.count = count;

      return count;
    }
    return -1;
  }
  return -1;
}

function getTableColumnTypes(tableName) {
  const result = [];
  const sel = db.prepare(`PRAGMA table_info('${tableName}')`);

  while (sel.step()) {
    const obj = sel.getAsObject();
    result[obj.name] = obj.type;
  }

  return result;
}

function getTableNameFromQuery(query) {
  const sqlRegex = SQL_FROM_REGEX.exec(query);
  if (sqlRegex != null) {
    return sqlRegex[1].replace(/"|'/gi, "");
  }
  return null;
}

function parseLimitFromQuery(query) {
  const sqlRegex = SQL_LIMIT_REGEX.exec(query);
  if (sqlRegex != null) {
    const result = {};

    if (sqlRegex.length > 2 && typeof sqlRegex[2] !== "undefined") {
      result.offset = parseInt(sqlRegex[1], 10);
      result.max = parseInt(sqlRegex[2], 10);
    } else {
      result.offset = 0;
      result.max = parseInt(sqlRegex[1], 10);
    }

    if (result.max === 0) {
      result.pages = 0;
      result.currentPage = 0;
      return result;
    }

    const queryRowsCount = getQueryRowCount(query);
    if (queryRowsCount !== -1) {
      result.pages = Math.ceil(queryRowsCount / result.max);
    }
    result.currentPage = Math.floor(result.offset / result.max) + 1;
    result.rowCount = queryRowsCount;

    return result;
  }
  return null;
}

export default class SqliteView {
  constructor(elementId, { choicesConfig = {} } = {}) {
    this.elementId = elementId;
    this.choicesConfig = { ...choicesConfig, ...{ allowHTML: true } };
    this.editorId = String(Math.floor(Math.random() * Math.floor(10)));

    // Return existing instance if possible.
    if (elementId in instances) {
      return instances[elementId];
    }
    instances[elementId] = this;

    // Initialize css
    this.css = document.createElement("style");
    this.css.setAttribute("id", "sqlview-css");
    this.css.innerHTML = styles;

    // Initialize html
    this.element = document.createElement("div");
    this.element.setAttribute("id", elementId);
    this.element.innerHTML = html(this.editorId);
  }

  load(database) {
    return new Promise((resolve, reject) => {
      // Insert css
      if (document.getElementById("sqlview-css") == null) {
        document.head.prepend(this.css);
      }

      // Insert html
      document.getElementById(this.elementId).replaceWith(this.element);

      // Create event handlers
      this.element.querySelector(".sqlview-editor-container > div").onkeydown =
        (e) => {
          if ((e.ctrlKey || e.metaKey) && e.which === 13) {
            this.executeSql();
          }
        };
      this.element.querySelector(
        ".sqlview-run-button-container > button",
      ).onclick = () => this.executeSql();
      this.element.querySelector(".sqlview-page-prev").onclick = (e) =>
        this.setPage(e.srcElement, false);
      this.element.querySelector(".sqlview-pager").onclick = (e) =>
        this.setPage(e.srcElement);
      this.element.querySelector(".sqlview-page-next").onclick = (e) =>
        this.setPage(e.srcElement, true);

      // Initialize editor
      this.editor = ace.edit(this.editorId);
      this.editor.setTheme("ace/theme/chrome");
      this.editor.renderer.setShowGutter(false);
      this.editor.renderer.setShowPrintMargin(false);
      this.editor.renderer.setPadding(20);
      this.editor.renderer.setScrollMargin(8, 8, 0, 0);
      this.editor.setHighlightActiveLine(false);
      this.editor.getSession().setUseWrapMode(true);
      this.editor.getSession().setMode("ace/mode/sql");
      this.editor.setOptions({ maxLines: 5 });
      this.editor.setFontSize(16);

      // Load data
      const load = (arrayBuffer) => {
        this.loadDB(arrayBuffer).then(() => {
          resolve(this);
        });
      };
      function loadFromUrl(url) {
        const oReq = new XMLHttpRequest();
        oReq.open("GET", url, true);
        oReq.responseType = "arraybuffer";
        oReq.onload = () => load(oReq.response);
        oReq.send(null);
      }
      switch (true) {
        case database instanceof ArrayBuffer:
          load(database);
          break;
        case typeof database === "string":
          loadFromUrl(database);
          break;
        default:
          reject(Error(`Sqlite database of unknown type: ${typeof database}.`));
      }

      // Update pager position
      window.onresize = () => this.windowResize();
      window.onscroll = () => this.positionFooter();
      this.windowResize();
    });
  }

  loadDB(arrayBuffer) {
    this.resetTableList();

    return initSqlJs({ locateFile: () => sqljsPath }).then((SQL) => {
      db = new SQL.Database(new Uint8Array(arrayBuffer));

      // Get all table names from master table
      const tables = db.prepare(
        "SELECT * FROM sqlite_master WHERE type='table' OR type='view' ORDER BY name",
      );

      let firstTableName = null;
      const choices = [];

      while (tables.step()) {
        const { name } = tables.getAsObject();

        if (firstTableName === null) {
          firstTableName = name;
        }
        const rowCount = getTableRowsCount(name);

        choices.push({
          value: name,
          label: `${escapeHTML(name)} <span style="color:#ccc">(${rowCount} rows)</span>`,
        });
      }

      this.tableSelector = new Choices(
        ".sqlview-table-select-container > select",
        { ...{ choices }, ...this.choicesConfig },
      );

      // Select first table and show it
      this.tableSelector.setChoiceByValue(firstTableName);
      this.doDefaultSelect(firstTableName);

      this.element.querySelector(
        ".sqlview-main-container > div",
      ).style.animation = "sqlview-fadein 1s linear";
    });
  }

  resetTableList() {
    const selectContainer = this.element.querySelector(
      ".sqlview-table-select-container",
    );
    selectContainer.innerHTML = `
      <select title="Table">
        <option value="">Select a table</option>
      </select>
      <br />
    `;
    selectContainer.querySelector("select").onchange = (e) =>
      this.doDefaultSelect(e.target.value);
  }

  doDefaultSelect(name) {
    const defaultSelect = `SELECT * FROM '${name}' LIMIT 0,30`;
    this.editor.setValue(defaultSelect, -1);
    this.renderQuery(defaultSelect);
  }

  setPage(el, next) {
    if (el.classList.contains("disabled")) {
      return;
    }

    const query = this.editor.getValue();
    const limit = parseLimitFromQuery(query);

    let pageToSet;
    if (typeof next !== "undefined") {
      pageToSet = next ? limit.currentPage : limit.currentPage - 2;
    } else {
      const page = prompt("Go to page");
      if (!Number.isNaN(page) && page >= 1 && page <= limit.pages) {
        pageToSet = page - 1;
      } else {
        return;
      }
    }

    const offset = pageToSet * limit.max;
    this.editor.setValue(
      query.replace(SQL_LIMIT_REGEX, `LIMIT ${offset},${limit.max}`),
      -1,
    );

    this.executeSql();
  }

  executeSql() {
    const query = this.editor.getValue();
    this.renderQuery(query);
    this.tableSelector.setChoiceByValue(getTableNameFromQuery(query));
  }

  renderQuery(query) {
    const dataBox = this.element.querySelector("table");
    const errorBox = this.element.querySelector(".sqlview-error");
    const thead = dataBox.querySelector("thead tr");
    const tbody = dataBox.querySelector("tbody");

    thead.innerHTML = "";
    tbody.innerHTML = "";
    errorBox.style.display = "none";
    dataBox.style.display = "table";

    let columnTypes = [];
    const tableName = getTableNameFromQuery(query);
    if (tableName != null) {
      columnTypes = getTableColumnTypes(tableName);
    }

    let sel;
    try {
      sel = db.prepare(query);
    } catch (ex) {
      dataBox.style.display = "none";
      this.element.querySelector(".sqlview-bottom-bar").style.display = "none";
      errorBox.style.display = "block";
      errorBox.innerHTML = ex;
      return;
    }

    let addedColums = false;
    while (sel.step()) {
      if (!addedColums) {
        addedColums = true;
        const columnNames = sel.getColumnNames();
        for (let i = 0; i < columnNames.length; i += 1) {
          const type = columnTypes[columnNames[i]];
          thead.insertAdjacentHTML(
            "beforeend",
            `<th><span data-toggle="tooltip" data-placement="top" title="${escapeHTML(type)}">${escapeHTML(columnNames[i])}</span></th>`,
          );
        }
      }

      let tr = "<tr>";
      const s = sel.get();
      for (let i = 0; i < s.length; i += 1) {
        const cell = escapeHTML(s[i]);
        tr += `<td><span title="${cell}">${cell}</span></td>`;
      }
      tr += "</tr>";
      tbody.insertAdjacentHTML("beforeend", tr);
    }

    this.refreshPagination(query);

    setTimeout(() => this.positionFooter(), 100);
  }

  windowResize() {
    this.positionFooter();
    const container = this.element.querySelector(".sqlview-main-container");
    const cleft =
      container.getBoundingClientRect().top +
      window.scrollY +
      container.offsetWidth;
    this.element.querySelector(".sqlview-bottom-bar").style.left = cleft;
  }

  positionFooter() {
    const footer = this.element.querySelector(".sqlview-bottom-bar");
    const pager = footer.querySelector(".sqlview-pager");
    const containerHeight = this.element.querySelector(
      ".sqlview-main-container",
    ).clientHeight;
    const footerTop = window.scrollY + window.clientHeight;

    if (bottomBarDefaultPos === null) {
      bottomBarDefaultPos = footer.style.position;
    }

    if (bottomBarDisplayStyle === null) {
      bottomBarDisplayStyle = pager.style.display;
    }

    if (footerTop > containerHeight) {
      footer.style.position = "static";
      pager.style.display = "inline-block";
    } else {
      footer.style.position = bottomBarDefaultPos;
      pager.style.display = bottomBarDisplayStyle;
    }
  }

  refreshPagination(query) {
    const limit = parseLimitFromQuery(query);
    const bottomBar = this.element.querySelector(".sqlview-bottom-bar");
    const pagePrev = this.element.querySelector(".sqlview-page-prev");
    const pageNext = this.element.querySelector(".sqlview-page-next");
    if (limit !== null && limit.pages > 0) {
      const pager = this.element.querySelector(".sqlview-pager");
      pager.setAttribute("title", `Row count: ${limit.rowCount}`);
      pager.innerHTML = `${limit.currentPage} / ${limit.pages}`;

      if (limit.currentPage <= 1) {
        pagePrev.classList.add("disabled");
      } else {
        pagePrev.classList.remove("disabled");
      }

      if (limit.currentPage + 1 > limit.pages) {
        pageNext.classList.add("disabled");
      } else {
        pageNext.classList.remove("disabled");
      }
      bottomBar.style.display = "block";
    } else {
      bottomBar.style.display = "none";
    }
  }
}
