import SqliteView from "../../../src/index.js";

$.urlParam = function (name) {
  var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
    window.location.href,
  );
  if (results == null) {
    return null;
  } else {
    return results[1] || 0;
  }
};

async function loadDatabase(sqlite) {
  const view = new SqliteView(
    "output-box",
    // Copied from: https://github.com/jshjohnson/Choices/issues/828#issuecomment-656777171
    {
      choicesConfig: {
        classNames: {
          containerInner: "choices__inner bg-secondary border-0",
          input: "form-control",
          item: "choices__item bg-secondary",
          highlightedState: "text-info",
          selectedState: "text-info",
        },
      },
    },
  );
  const bootstrapClasses = {
    button: ["btn"],
    ".sqlview-bottom-bar": ["text-center"],
    ".sqlview-bottom-bar button": ["btn-default"],
    ".sqlview-error": ["alert", "alert-danger", "box"],
    ".sqlview-main-container button": ["btn-sm", "btn-primary"],
    ".sqlview-main-container > div": ["container-fluid"],
    ".sqlview-main-container > div > div": ["row"],
    ".sqlview-editor-container > div": [
      "panel",
      "panel-default",
      "form-control",
      "form-control-lg",
    ],
    ".sqlview-table-select-container > select": ["form-control"],
    ".sqlview-table-select-container": ["col-md-12"],
    ".sqlview-editor-container": ["col-md-11"],
    ".sqlview-run-button-container": ["col-sm-1"],
    ".sqlview-table-container": ["col-md-12"],
    ".sqlview-table-container table": [
      "table",
      "table-condensed",
      "table-bordered",
      "table-hover",
      "table-striped",
    ],
  };
  Object.entries(bootstrapClasses).forEach(([selector, classes]) => {
    view.element.querySelectorAll(selector).forEach((e) => {
      e.classList.add(...classes);
    });
  });
  await view.load(sqlite);
  $(".sqlview-table-container table").editableTableWidget();
  $("#output-box").fadeIn();
  $(".nouploadinfo").hide();
  $("#sample-db-link").hide();
  $("#dropzone").delay(50).animate({ height: 50 }, 500);
  $("#success-box").show();
}

//Check url to load remote DB
var loadUrlDB = $.urlParam("url");
if (loadUrlDB != null) {
  loadDatabase(loadUrlDB);
}

var fileReaderOpts = {
  readAsDefault: "ArrayBuffer",
  on: {
    load: function (e, file) {
      loadDatabase(e.target.result);
    },
  },
};

var selectFormatter = function (item) {
  var index = item.text.indexOf("(");
  if (index > -1) {
    var name = item.text.substring(0, index);
    return (
      name +
      '<span style="color:#ccc">' +
      item.text.substring(index - 1) +
      "</span>"
    );
  } else {
    return item.text;
  }
};

var toggleFullScreen = function () {
  var container = $("#main-container");
  var resizerIcon = $("#resizer i");

  container.toggleClass("container container-fluid");
  resizerIcon.toggleClass("glyphicon-resize-full glyphicon-resize-small");
};
$("#resizer").click(toggleFullScreen);

if (typeof FileReader === "undefined") {
  $("#dropzone, #dropzone-dialog").hide();
  $("#compat-error").show();
} else {
  $("#dropzone, #dropzone-dialog").fileReaderJS(fileReaderOpts);
}

$(".no-propagate").on("click", function (el) {
  el.stopPropagation();
});

function setIsLoading(isLoading) {
  var dropText = $("#drop-text");
  var loading = $("#drop-loading");
  if (isLoading) {
    dropText.hide();
    loading.show();
  } else {
    dropText.show();
    loading.hide();
  }
}

function extractFileNameWithoutExt(filename) {
  var dotIndex = filename.lastIndexOf(".");
  if (dotIndex > -1) {
    return filename.substr(0, dotIndex);
  } else {
    return filename;
  }
}

window.dropzoneClick = () => $("#dropzone-dialog").click();
