[![npm](https://img.shields.io/npm/v/sqlite-view)](https://www.npmjs.com/package/sqlite-view)

A library for building sqlite readers in web applications.

This project emerged as an abstraction of the [sqlite-viewer](https://inloop.github.io/sqlite-viewer/) application.

See [demos](https://ryneeverett.gitlab.io/sqlite-view).

# Installation

```shell
npm install sqlite-view
```

# Usage

```html
<div id="sqlite-viewer"></div>

<script type="module">
  import SqliteView from "sqlite-view";
  const viewer = new SqliteView("sqlite-view");
  viewer.load("/path/to/db.sqlite");
</script>
```

# Quick Start

```html
<div id="sqlite-viewer"></div>

<script type="module">
  import SqliteView from "https://unpkg.com/sqlite-view/sqlite-view.js";
  const viewer = new SqliteView("sqlite-viewer");
  viewer.load(
    "https://ryneeverett.gitlab.io/sqlite-view/sqlite-viewer/examples/Chinook_Sqlite.sqlite",
  );
</script>
```

# Building from Source

```sh
npm install sqlite-view vite
export PATH="$PATH:$PWD/node_modules/.bin"
vite build node_modules/sqlite-view --outDir "$(pwd)/sqlite-view"
```

# API

## SqliteView(element, config)

### element

The string id of an existing element in the DOM where sqlite-view will inject the reader.

### config

An optional object of configurations:

**choicesConfig**: An object of which is passed directly to [Choices configuration](https://github.com/jshjohnson/Choices#configuration-options).

## SqliteView.load(database)

### database

Either a string url path or a ByteArray of a database.

# Development

## Installing development environment

### With nix:

```shell
nix-shell
```

### Without nix:

```shell
npm install
```

## Running examples

```shell
npm run serve
```

## Running build and tests

```shell
npm test
```

This runs the build, unit tests, integration tests, and other checks. The main difference between the two test suites is that the unit tests are run from within the browser context whereas the integration tests are run from outside the browser context (nodejs server).

## Publishing a new version to NPM

```
npm version <version>
```

## Security

Database _contents_ are escaped before injection in order to mitigate XSS.

However, databases of untrusted construction could perform sql injection via table names. Sqlite does not support table name parameterization and sqlite does not have any restrictions on table name validity, so there is necessarily a trade-off between supporting all valid sqlite databases and avoiding sql injection. Currently the former is chosen and no escaping of table names is done. It's likely that sql.js also does not have the threat model of maliciously-constructed databases in mind and that even with mitigations in place it would still be insecure to load untrusted databases.
