# TODO Pin nixpkgs via niv or (more likely at this point) flakes.
with import <nixpkgs> {};
mkShell {
  shellHook = ''
    set -ex

    # If not using direnv we can change the environment in the shellHook.
    if [ $DIRENV_DIR = \\'\\\'\ ]; then
      export PATH="$PATH:$(pwd)/node_modules/.bin"
    fi

    ln -s --force ${pkgs.chromium}/bin/chromium ./.chromium

    # Don't update packages more than once a day.
    if [ $(date +%D -d "@$(stat -c %Y package-lock.json)") != $(date +%D) ]; then
      # Update packag-lock.json but not package.json.
      cp package.json package.json.bak
      npm install
      npm update --include=dev
      mv package.json.bak package.json
    fi

    set +ex
  '';

  PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD=1;

  buildInputs = with pkgs; [
    nodejs
  ];
}
